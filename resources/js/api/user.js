import { ROAST_CONFIG } from '../config.js';

export default {
    getUser: function () {
        return axios.get(ROAST_CONFIG.API_URL + '/user');
    }
}