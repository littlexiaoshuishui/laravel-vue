/**
 * Imports the Roast API URL from the config.
 */
import { ROAST_CONFIG } from '../config.js';

export default {
    /**
     * GET /api/v1/cafes
     */
    getCafes: function(){
        return axios.get( ROAST_CONFIG.API_URL + '/cafes' );
    },

    /**
     * GET /api/v1/cafes/{cafeID}
     */
    getCafe: function( cafeID ){
        return axios.get( ROAST_CONFIG.API_URL + '/cafes/' + cafeID );
    },

    /**
     * POST /api/v1/cafes
     */
    postAddNewCafe: function (name, locations, website, description, roaster) {
        return axios.post(ROAST_CONFIG.API_URL + '/cafes',
            {
                name: name,
                locations: locations,
                website: website,
                description: description,
                roaster: roaster
            }
        );
    },

    /**咖啡店添加喜欢标签
     * /api/v1/cafes/3/like
     * @param $cafe_id
     * @returns
     */
    postLikeCafe: function ($cafe_id){
        return axios.post(ROAST_CONFIG.API_URL+'/cafes/'+$cafe_id+'/like');
    },
    /**咖啡店取消喜欢标签
     * /api/v1/cafes/3/like
     * @param $cafe_id
     * @returns {*}
     */
    deleteLikeCafe: function ($cafe_id) {
        return axios.delete(ROAST_CONFIG.API_URL+'/cafes/'+$cafe_id+'/like');
    }

}