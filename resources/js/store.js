/**
 * Import Vue and Vuex
 */
import Vue from 'vue'
import Vuex from 'vuex'
/**
 * Initializes Vuex on Vue.
 */
Vue.use( Vuex )

/**
 * 引入cafes模块，一个模块表示一个页面的数据
 */
import { cafes } from './modules/cafe.js';
import { brewMethods } from './modules/brewMethods.js';


/**
 * Export the data store.
 */
export default new Vuex.Store({
    modules: {
        cafes,
        brewMethods
    }
});