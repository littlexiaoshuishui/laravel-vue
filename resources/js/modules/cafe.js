/*
|-------------------------------------------------------------------------------
| VUEX modules/cafes.js
|-------------------------------------------------------------------------------
| The Vuex data store for the cafes
status = 0 -> 数据尚未加载
status = 1 -> 数据开始加载
status = 2 -> 数据加载成功
status = 3 -> 数据加载失败
*/

import CafeAPI from '../api/cafe.js';

export const cafes = {
    state:{
        cafes:[],
        cafesLoadStatus:0,
        cafe:{},
        cafeLoadStatus:0,
        cafeAddStatus: 0,
        cafeLikeActionStatus:0,
        cafeUnlikeActionStatus:0,
        cafeLiked:false
    },
    actions:{

        loadCafes( { commit } ){
            commit( 'setCafesLoadStatus', 1 );

            CafeAPI.getCafes()
                .then( function( response ){
                    commit( 'setCafes', response.data );
                    commit( 'setCafesLoadStatus', 2 );
                })
                .catch( function(){
                    commit( 'setCafes', [] );
                    commit( 'setCafesLoadStatus', 3 );
                });
        },
        loadCafe( { commit }, data ){
            commit('setCafe',1);
            CafeAPI.getCafe(data.id).then(function(response){
                commit('setCafe',response.data);
                commit('setCafeLoadStatus',2);
                if (response.data.user_like.length>0) {
                    commit('setCafeLiked', true);
                }
            }).catch(function(){
                commit('setCafe',{});
                commit('setCafeLoadStatus',3)
            });
        },
        addCafe({commit, state, dispatch}, data) {
            commit('setCafeAddStatus', 1);

            CafeAPI.postAddNewCafe(data.name, data.locations, data.website, data.description, data.roaster)
                .then(function (response) {
                    commit('setCafeAddStatus', 2);
                    dispatch('loadCafes');
                })
                .catch(function () {
                    commit('setCafeAddStatus', 3);
                });
        },
        likeCafe({commit,state}, data) {
            commit('setCafeLikeActionStatus',1);
            CafeAPI.postLikeCafe(data.id).then(function (response) {
                commit('setCafeLikeActionStatus',2);
                commit('setCafeLiked', true);
            }).catch(function (response) {
                commit('setCafeLikeActionStatus',3);
            });
        },
        unlikeCafe({commit,state}, data){
            commit('setCafeUnlikeActionStatus',1);
            CafeAPI.deleteLikeCafe(data.id).then(function (response) {
                commit('setCafeUnlikeActionStatus',2);
                commit('setCafeLiked', false);
            }).catch(function (response) {
                commit('setCafeUnlikeActionStatus',3);
            });
        }

    },
    mutations:{
        setCafesLoadStatus(state,status){
            state.cafesLoadStatus = status;
        },
        setCafes(state,cafes){
            state.cafes = cafes;
        },
        setCafeLoadStatus(state,status){
            state.cafeLoadStatus = status;
        },
        setCafe(state,cafe){
            state.cafe = cafe;
        },
        setCafeAddStatus(state,status){
            state.cafeAddStatus = status;
        },
        setCafeLikeActionStatus(state,status){
            state.cafeLikeActionStatus = status;
        },
        setCafeLiked(state,status){
            state.cafeLiked = status;
        },
        setCafeUnlikeActionStatus(state,status){
            state.cafeUnlikeActionStatus = status;
        }
    },
    getters:{
        getCafes(state){
            return state.cafes;
        },
        getCafesLoadStatus(state){
            return state.cafesLoadStatus;
        },
        getCafe(state){
            return state.cafe;
        },
        getCafeLoadStatus(state){
            return state.cafeLoadStatus;
        },
        getAddCafeStatus(state){
            return state.cafeAddStatus;
        },
        getCafeLikeActionStatus(state){
            return state.cafeLikeActionStatus;
        },
        getCafeUnlikeActionStatus(state){
            return state.cafeUnlikeActionStatus;
        },
        getCafeLiked(state){
            return state.cafeLiked;
        }
    }

}