import BrewMethodApi from '../api/brewMethod.js';

export const brewMethods = {
    state: {
        brewMethods: [],
        brewMethodsLoadStatus: 0
    },
    actions: {
        loadBrewMethods({commit}){
            commit('setBrewMethodsLoadStatus', 1);
            BrewMethodApi.getBrewMethods().then(function(response) {
                commit('setBrewMethodsLoadStatus', 2);
                commit('setBrewMethods', response.data);
            }).catch(function(response) {
                commit('setBrewMethodsLoadStatus', 3);
            });
        }
    },
    mutations: {
        setBrewMethodsLoadStatus(state, status) {
            state.brewMethodsLoadStatus = status;
        },
        setBrewMethods(state, brewMethods) {
            state.brewMethods = brewMethods;
        }
    },
    getters: {
        getBrewMethods(state) {
            return state.brewMethods;
        },
        getBrewMethodsLoadStatus(state) {
            return state.brewMethodsLoadStatus;
        }
    }
}