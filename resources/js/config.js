/**
 * Defines the API route we are using.
 */
var api_url = '';
var app_url = '';
var gaode_maps_js_api_key = '333bf27f694ae0b617e9647a7ee02291';

switch( process.env.NODE_ENV ){
    case 'development':
        api_url = 'http://laravel-vue.test/api/v1';
        app_url = "http://laravel-vue.test";
        break;
    case 'production':
        api_url = 'http://laravel.laravelacademy.org/api/v1';
        break;
}

export const ROAST_CONFIG = {
    API_URL: api_url,
    APP_URL: app_url,
    GAODE_MAPS_JS_API_KEY: gaode_maps_js_api_key
};