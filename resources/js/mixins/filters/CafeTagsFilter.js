export const CafeTagsFilter = {
    methods: {
        processCafeTagsFilter(cafe, tags) {
            if(tags.length>0){
                var cafeTags = [];
                for(var i=0; i<cafe.tags.length; i++){
                    cafeTags.push(cafe.tags[i]);
                }
                for(var i=0; i<tags.length; i++){
                    if (cafeTags.indexOf(tags[i])) {
                        return true;
                    }else{
                        return false;
                    }
                }
            }else{
                return true;
            }
        }
    }
}