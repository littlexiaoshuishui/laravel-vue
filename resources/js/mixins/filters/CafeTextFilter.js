export const CafeTextFilter = {
    methods: {
        processCafeTextFilter (cafe,text) {
            if(text.length>0) {
                //如果咖啡店名称、位置、地址或城市与筛选文本匹配，则返回 true，否则返回 false
                if ( cafe.name.toLowerCase().match('[^,]*' + text.toLowerCase() + '[,$]*')
                    ||cafe.location_name.toLowerCase().match('[^,]*' + text.toLowerCase() + '[,$]*')
                    ||cafe.location_name.toLowerCase().match('[^,]*' + text.toLowerCase() + '[,$]*')
                    ||cafe.city.toLowerCase().match('[^,]*' + text.toLowerCase() + '[,$]*')) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }
    }

}