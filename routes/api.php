<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|'middleware' => 'auth:api'
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//公有路由，不需要登陆
Route::group(['prefix' => 'v1'], function() {
    Route::get('/cafes', 'API\CafesController@getCafes');
    Route::get('/cafes/{id}', 'API\CafesController@getCafe');
    //获取应用中的所有冲泡方法
    Route::get('/brew-methods', 'API\BrewMethodsController@getBrewMethods');
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function(){
    Route::get('/user', 'API\UserController@getUser');

    //新增咖啡店
    Route::post('/cafes', 'API\CafesController@postNewCafe');
    // 喜欢咖啡店
    Route::post('/cafes/{id}/like', 'API\CafesController@postLikeCafe');
    // 取消喜欢咖啡店
    Route::delete('/cafes/{id}/like', 'API\CafesController@deleteLikeCafe');
    // 用户为某个咖啡店添加标签
    Route::post('/cafes/{id}/tags', 'API\CafesController@postAddTags');
    //用户从某个咖啡店上删除标签
    Route::delete('/cafes/{id}/tags/{tagName}', 'API\CafesController@deleteCafeTag');
    //根据查询的字符返回标签
    Route::get('/tags', 'API\TagsController@getTags');

});


