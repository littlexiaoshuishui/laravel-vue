<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//单页面应用路径
Route::get('/', 'Web\AppController@getApp');


//单页面登陆授权路径，使用guest中间件表示，登陆用户访问这个路径，会自动跳到登陆成功的页面
//Route::get('/login', 'Web\AppController@getLogin' )
//    ->name('login')
//    ->middleware('guest');

Route::get( '/auth/{social}', 'Web\AuthenticationController@getSocialRedirect' )
    ->middleware('guest');

Route::get( '/auth/{social}/callback', 'Web\AuthenticationController@getSocialCallback' )
    ->middleware('guest');

Route::get('geocode', function () {
    return \App\Utilities\GaodeMaps::geocodeAddress('白云区同和大道', '广州', '广东');
});

Route::get('/cafes/{id}', 'API\CafesController@getCafe');