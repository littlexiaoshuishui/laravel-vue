<?php

namespace App\Utilities;

use App\Models\Tag;

class Tagger
{
    /**为某个咖啡店新增标签
     * @param $cafe model
     * @param $tags [标签数组]
     * @param $user_id [添加人]
     */
    public static function tagCafe($cafe, $tags, $user_id)
    {
        foreach($tags as $name) {
            //查询这个标签，不存在就new
            $tag = Tag::firstOrNew(['name'=>$name]);
            $tag->name = $name;
            $tag->save();
            //往中间表插入一条数据
            $cafe->tags()->syncWithoutDetaching([
                $tag->id => ['user_id'=>$user_id]
            ]);

        }
    }
}




















?>