<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;

class TagsController extends Controller
{
    /**
     * 按照查询条件获取咖啡店标签
     */
    public function getTags()
    {
        $query = Request::get('search');

        if ($query == null || $query == '') {
            $tags = Tag::all();
        } else {
            $tags = Tag::where('name', 'LIKE', $query . '%')->get();
        }

        return response()->json($tags);
    }
}
