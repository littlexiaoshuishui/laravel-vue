<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * 获取登陆用户信息
     */
    public function getUser()
    {
        return Auth::guard('api')->user();
    }
}
