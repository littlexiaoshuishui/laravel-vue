<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cafe extends Model
{
    //定义与BrewMethod模型的多对多的关联关系方法
    public function brewMethods()
    {
        return $this->belongsToMany(BrewMethod::class, 'cafes_brew_methods', 'cafe_id', 'brew_method_id');
    }

    // 关联分店 自己与自己关联
    public function children()
    {
        return $this->hasMany(Cafe::class, 'parent', 'id');
    }

    // 归属总店 自己与自己关联
    public function parent()
    {
        return $this->hasOne(Cafe::class, 'id', 'parent');
    }

    //与user表多对多关系，中间表user_cafes_likes,用户喜欢的咖啡店记录表
    public function likes()
    {
        return $this->belongsToMany(User::class,'users_cafes_likes','cafe_id','user_id');
    }

    //查询登陆用户喜欢的咖啡店
    public function userLike()
    {
        return $this->belongsToMany(User::class,'users_cafes_likes','cafe_id','user_id')->where('user_id','=',auth()->id());
    }

    /**
     * 与tags表关联，查询这个咖啡店下面的所有标签
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class,'cafes_users_tags','cafe_id','tag_id');
    }
}
