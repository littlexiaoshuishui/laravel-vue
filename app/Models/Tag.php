<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * 与咖啡店的关联方法，查询这个标签下的咖啡店
     */
    public function cafes()
    {
        return $this->belongsToMany(Cafe::class,'cafes_users_tags','tag_id','user_id');
    }
}
